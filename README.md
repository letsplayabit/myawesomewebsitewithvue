---
marp: true
theme: your-theme
# class: invert
paginate: true
# style: |
#     section {
#         background-color: yellow;
#     }
---

# Die eigene State-of-the-Art Website
oder zielgerichtetes googeln

Christoph Böhm

---

# Was will ich erreichen (funktional)
- Ich will eine (private) Website
- Ich will eigene Inhalte (Posts) einstellen

---

# Was will ich erreichen (nicht-funktional)
- Ich will günstig.
- Ich will lernen.
- Ich will wenig Aufwand.
- Ich will bereits vorhandenes Nutzen.

---


# Und natürlich fancy
![bg right:40%](./slides/img/fancy.png)

aber das machen wir später ...


---

# pre-requisites
## GitLab Account
![height:450px](./slides/img/gitlabAccount.png)

---

# Wie fange ich denn an
Lasset uns googeln.

Google: Webentwicklung
https://de.wikipedia.org/wiki/Webentwicklung

![height:400px](./slides/img/history_web.png)


---

Google: JAMStack

JAM is short for: JavaScript, APIs, and Markup. We use JavaScript to load data from APIs and combine it with markup or static files to render static web pages.

https://www.netlify.com/
https://jamstack.wtf/


---

# Wie bewerte ich eine Technologie

## StackShare
https://stackshare.io/stacks
- TechStacks von Unternehmen
- Durchdringung der Technologie

## Stackoverflow
https://insights.stackoverflow.com/trends?tags=jquery%2Cangularjs%2Cangular%2Creactjs%2Cvue.js

---

## Github Stars & Folks
![height:300px](./slides/img/githubStars.png)

https://github.com/search?o=desc&q=topic%3Ajamstack&s=stars&type=Repositories

---

# Los geht's

https://gridsome.org/docs/

Node.js installieren
https://nodejs.org/

Google: variants install nodejs

Node Version Manager installieren
https://github.com/nvm-sh/nvm
~~~ bash
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
~~~

Node.js installieren
~~~ bash
nvm install latest # 14.15.1
~~~

---

Gridsome CLI Tool installieren
~~~ bash
npm install --global @gridsome/cli`
~~~

# Projekt erstellen

~~~ bash
gridsome create myAweSomeWebsiteWithVue
cd myAweSomeWebsiteWithVue
gridsome develop to start a local dev server at `http://localhost:8080`
~~~

---

# Versionsverwaltung einrichten
~~~ bash
git init
git remote add gitlab https://gitlab.com/letsplayabit/myawesomewebsitewithvue
git add .
git commit -m "first commit"
git push --set-upstream gitlab master
~~~

# Deploy
https://gridsome.org/docs/deploy-to-gitlab/

gridsome.config.js
~~~ yml
  siteName: 'myAwesomeWebsiteWithVue',
  siteUrl: 'https://letsplayabit.gitlab.io/myawesomewebsitewithvue/',
  pathPrefix: '/', // ggf. /myawesomewebsitewithvue/
  outputDir: 'public',
~~~


---

Gitlab Pipeline erstellen. (Node Version beachten)

.gitlab-ci.yml
~~~ yml
image: node:14.15.1

pages:
  cache:
    paths:
      - node_modules/
  script:
    - npm install
    - npm run build
  artifacts:
    paths:
      - public
  only:
    - master
~~~


---

## Howto find the Url
https://gitlab.com/<username>/<projectName>/pages

## Ich sehe nichts
Berechtigungen ändern
![height:150px](./slides/img/visibility_pages.png)
https://gitlab.com/<username>/<projectName>/edit#js-general-project-settings
➡ Visibility, project features, permissions
➡ Pages

---

## Ein bisschen (variabler) Inhalt
Index.vue
~~~ html
<template>
  <Layout>
    ...
    <h1>Hello {{ unit }}</h1>
    ...
  </Layout>
</template>
<script>
// ...
  data: function() {
      return {
        unit: 'IT'
      }
  }
}
</script>
~~~

---

# Irgendwas mit Daten
Daten abrufen / GraphQL
http://localhost:8080/___explore

---

## Dateizugriff
https://gridsome.org/docs/fetching-data/#import-with-source-plugins

zusätzliches Package (automatisch in package.json eingetragen)

~~~ bash
npm install @gridsome/source-filesystem
~~~

gridsome.config.js
~~~ js
plugins: [
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/posts/**/*.md',
        typeName: 'Post',
        route: '/blog/:slug'
      }
    }
]
~~~

---

## Markdown Support
Was ist denn das?
https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet


https://gridsome.org/plugins/@gridsome/transformer-remark
npm install @gridsome/transformer-remark

gridsome.config.js
~~~ js
  transformers: {
    remark: {
      // global remark options
    }
  }
~~~

---


# Unser erster Post
ersterPost.md
~~~ m
---
title: "Hello World"
description: "the first and maybe only post."
date: 2019-05-23
---

Ich kenn ein Lied und das geht jedem auf die Nerven.
~~~

# Und was ist mit Bildern?
~~~ m
![alt text](./img/logo-normal-dark.svg "Das ist das Logo von Gridsome")
~~~

---

# Neue Route
Default.vue

~~~
<g-link class="nav__link" to="/blog/">Blog</g-link>
~~~

---

# Geht das auch in schön?
:magic_wand: Zauberwort: css
https://github.com/search?o=desc&q=topic%3Acss&s=stars&type=Repositories
Ergebnis:
- Bootstrap (150k :star:)
- Bulma (43,6k :star:)
- tailwind (44,2k :star:)

---

# Tailwind
Installation:
https://gridsome.org/docs/assets-css/#add-a-css-framework

~~~ bash
npm install -D tailwindcss@npm:@tailwindcss/postcss7-compat postcss@^7 autoprefixer@^9
npx tailwind init
~~~

Probleme mit Tailwind Plugin und/ oder PostCSS
https://tailwindcss.com/docs/installation#post-css-7-compatibility-build
~~~ bash
npm uninstall tailwindcss postcss autoprefixer
npm install -D tailwindcss@npm:@tailwindcss/postcss7-compat postcss@^7 autoprefixer@^9
~~~

---


---
Weitere Einrichtung
https://mannes.tech/gridsome-tailwindcss/

---


# Jetzt wirds bunt
Konfiguration:
https://tailwindcss.com/docs/configuration

## Aller Farben sind schwer
tailwind.config.js in root
~~~ js
const colors = require('tailwindcss/colors')

module.exports = {
 ...

  theme: {
    colors: {
      gray: colors.coolGray,
      blue: colors.lightBlue,
      green: colors.green,
      red: colors.rose,
      pink: colors.fuchsia,
    },
  },

...

}
~~~

## Komponenten designen
https://play.tailwindcss.com/
https://tailwindui.com/
https://nerdcave.com/tailwind-cheat-sheet


---

# Your try

Source des Projektes und des Vortrages.
https://gitlab.com/letsplayabit/myawesomewebsitewithvue

Einfach folken und weiterspielen.

---

# Innoq Meetup zum Thema
![height:400px](./slides/img/meetup_innoq.png)

---

# Was mit Data (nicht zum Thema)

![height:400px](./slides/img/snowflake_conf.png)
https://www.snowflake.com/build/sn

---

# Links
## Markdown Cheatsheet
https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

## Slides mit Markdown & Marp erstellen
https://marp.app/
https://marketplace.visualstudio.com/items?itemName=marp-team.marp-vscode
https://github.com/marp-team/marpit/blob/main/docs/image-syntax.md

## Emoji Shortcodes
https://emojipedia.org/

---

## Static websites mit Vue
https://gridsome.org/


## Tailwind  & Cheatsheet
https://tailwindcss.com/
https://nerdcave.com/tailwind-cheat-sheet
https://t3n.de/news/tailwind-css-zukunft-heisst-1299218/
https://play.tailwindcss.com/
https://tailwindui.com/

---

## HTML & CSS lernen
https://developer.mozilla.org/de/docs/Learn

## JAMStack
https://www.netlify.com/oreilly-jamstack/
https://jamstack.wtf/

---

## Optimerung
Backlinks
https://www.backlink-tool.org

### Keyword Analyse
https://neilpatel.com/de/ubersuggest/
https://moz.com/products/pro
https://majestic.com/r


## Ebenfalls interessant
Techstack von StackOverflow
https://stackshare.io/stack-exchange/stack-overflow

https://stackshare.io/stacks