---
title: "Hello World"
description: "the first and maybe only post."
date: 2019-05-23
---

Ich kenn ein Lied und das geht jedem auf die Nerven.

![alt text](./img/logo-normal-dark.svg "Das ist das Logo von Gridsome")

~~~html
<p>Hallo
</p>
~~~

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

:star: