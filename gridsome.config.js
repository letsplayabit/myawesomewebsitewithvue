// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`
const tailwindcss = require('tailwindcss')
const autoprefixer = require('autoprefixer')
// const colors = require("tailwindcss/colors");

module.exports = {
  siteName: 'myAwesomeWebsiteWithVue',
  siteUrl: 'https://letsplayabit.gitlab.io/myawesomewebsitewithvue/',
  pathPrefix: '/myawesomewebsitewithvue/',
  outputDir: 'public',
  plugins: [
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/posts/**/*.md',
        typeName: 'Post'
      }
    }
  ],
  templates: {
    Post: '/blog/:title'
  },
  transformers: {
    remark: {
      // global remark options
    }
  },
  css: {
    loaderOptions: {
      postcss: {
        plugins: [
          tailwindcss,
          autoprefixer,
          // colors
        ]
      }
    }
  }
}
