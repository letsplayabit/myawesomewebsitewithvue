const colors = require('tailwindcss/colors')

module.exports = {
  purge: [
    './src/**/*.vue',
    './src/**/*.js',
    './src/**/*.jsx',
    './src/**/*.html',
    './src/**/*.pug',
    './src/**/*.md'
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      gray: colors.coolGray,
      blue: colors.lightBlue,
      green: colors.green,
      red: colors.rose,
      pink: colors.fuchsia,
      white: colors.white
    },
    extend: {
      gridTemplateColumns: {
        // Simple 16 column grid
       'myOwn': '25% auto 25%',
      }
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
